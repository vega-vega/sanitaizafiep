<?php

namespace SanitizerFiep;

use SanitizerFiep\Model\ModelInterface;

class Sanitizer
{
    private $configuration;

    /**
     * Sanitizer constructor.
     */
    public function __construct()
    {
        $string = file_get_contents("src/config/config.json");
        $this->configuration = json_decode($string, true);

        require_once("src\model\ModelInterface.php");
        foreach ($this->configuration as $value) {
            if (array_key_exists('class', $value['parse'])) {
                require_once($value['parse']['class']['path']);
            }
        }
    }

    public function start($jsonStr)
    {
        $json = json_decode($jsonStr, true);

        $arr = [];
        foreach ($json as $key => $value) {
            $arr[] = $this->parse($key, $value);
        }

        return $arr;
    }

    public function getStructure()
    {
        $structure = [];

        foreach ($this->configuration as $value) {
            if (array_key_exists('parse', $value)) {
                unset($value['parse']);
                $structure[] = $value;
            }
        }

        return $structure;
    }

    private function parse($key, $value)
    {
        if (!array_key_exists($key, $this->configuration)) {
            return [$key => "Undefined key"];
        }

        $parser = $this->configuration[$key];

        if (array_key_exists('class', $parser['parse'])) {
            $model = new $parser['parse']['class']['namespace']();
            return [$key => $model->start($value)];
        } else if (!array_key_exists('regex', $parser['parse'])) {
            return [$key => "Wrong parser"];
        } else if (preg_match($this->configuration[$key]['parse']['regex'], $value)) {
            return [$key => $value];
        }

        return [$key => "Wrong data"];
    }
}