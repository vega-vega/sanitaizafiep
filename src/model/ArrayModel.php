<?php


namespace SanitizerFiep\Model;


class ArrayModel implements ModelInterface
{
    public function start($string)
    {
        if (preg_match('/^({).*(})$/', $string)) {
            $chars = preg_split('/{([^}]+)}*/i', $string, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
            $chars = preg_split('/;/', $chars[0]);

            return $chars;
        }

        return ["Wrong value"];
    }
}