<?php


namespace SanitizerFiep\Model;

require_once("src\model\ModelInterface.php");

class StructureModel implements ModelInterface
{
    public function start($string)
    {
        preg_match_all('#(\w+)=([^&=]*)(?:&|$)#', $string, $matches, PREG_SET_ORDER);

        $result = array();
        $i = 0;
        foreach ($matches as $m) {
            list(, $key, $value) = $m;

            if (!strlen($value)) {
                $i = (int)$key;
            }
            else {
                $result[$i][$key] = $value;
            }
        }

        return $result;
    }
}