<?php


namespace SanitizerFiep\Model;

interface ModelInterface
{
    public function start($string);
}