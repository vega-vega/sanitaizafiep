<?php

use SanitizerFiep\Sanitizer;

require_once("src\Sanitizer.php");
header('Content-type: application/json');
$sanitizer = new Sanitizer();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    var_dump($sanitizer->start($_POST['json']));
} else {
    var_dump($sanitizer->getStructure());
}