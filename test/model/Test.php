<?php

use PHPUnit\Framework\TestCase;
use SanitizerFiep\Sanitizer;

require_once("src\Sanitizer.php");

class Test extends TestCase
{
    public function testInt()
    {
        $sanitizer = new Sanitizer();

        $createArr = $sanitizer->start('{"int":"123"}');
        $checkArr = [["int" => "123"]];
        $this->assertSame($checkArr, $createArr);
    }

    /**
     * @dataProvider allProvider
     */
    public function testAll($json, $arr)
    {
        $sanitizer = new Sanitizer();
        $createArr = $sanitizer->start($json);
        $this->assertSame($arr, $createArr);
    }

    public function testDoubleParam()
    {
        $sanitizer = new Sanitizer();

        $createArr = $sanitizer->start('{"int":"123","str":"qqweq"}');
        $checkArr = [["int" => "123"], ["str" => "qqweq"]];
        $this->assertSame($checkArr, $createArr);
    }

    public function allProvider()
    {
        return [
            ['{"intw":"123"}', [["intw" => "Undefined key"]]],
            ['{"int":"12w3"}', [["int" => "Wrong data"]]],
            ['{"str":"qqweq"}', [["str" => "qqweq"]]],
            ['{"double":"11.22"}', [["double" => "11.22"]]],
            ['{"phone_rus":"+79261234567"}', [["phone_rus" => "+79261234567"]]],
            ['{"array":"{a1;a2;a3}"}', [["array" => ["a1", "a2", "a3"]]]],
            ['{"structure":"0=&task=rose&duration=1.25&user=15&1=&task=daisy&duration=0.75&user=25&2=&task=orchid&duration=1.15&user=7"}', [["structure" => [["task" => "rose", "duration" => "1.25", "user" => "15"], ["task" => "daisy", "duration" => "0.75", "user" => "25"], ["task" => "orchid", "duration" => "1.15", "user" => "7"]]]]]
        ];
    }
}
